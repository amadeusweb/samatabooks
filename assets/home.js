if (typeof($) == 'undefined') $ = jQuery.noConflict();

$(document).ready(function() {
	$secs = 62; var ticker;
	var player = document.getElementById('homemp3');
	$('#splash').appendTo('body');
	$("#splash").click(function(e) {
		hideSplash();
		//e.preventDefault(); //else lalitha link doesnt work
	});
	$("#show-splash").click(function(e) {
		showSplash();
		e.preventDefault();
	});
	function countdownEnter(){
		if ($secs == 0)
		{
			$("#splash").trigger("click");
			return;
		}
		$("#splash-timer").html($secs + '...');
		$secs--;
	}
	function showSplash()
	{
		$secs = 62;
		$('#splash-image').height(($(window).height() - 190) + 'px');
		$("#main").hide();
			$("#masthead").hide(); $("#show-again").hide();
		$("#splash").show();
		player.play();
		player.currentTime = 0;
		countdownEnter();
		ticker = setInterval(countdownEnter, 1000);
	}
	function hideSplash()
	{
		//clearInterval(ticker); return;
		$("#splash").hide();
		$("#main").show();
		$('#post-area').masonry(); //NB: so masonry can compute tiles
			$("#masthead").show(); $("#show-again").show();
		player.pause();
		clearInterval(ticker);
	}
	if (window.pageName == 'index')
		showSplash();
});
