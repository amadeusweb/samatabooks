<li class="menu-item"><a class="menu-link" href="<?php echo am_var('url'); ?>"><div>Home</div></a></li>
<li class="menu-item"><a class="menu-link" href="<?php echo am_var('url'); ?>catalogue/"><div>Books</div></a>
	<ul class="sub-menu-container">
<?php render_menu([
	'bhagavad-gita/' => 'Bhagavad Gita',
	'the-essence-of-yogavasishta/' => 'Yogavasishta',
	'prabodhasudhakara/' => 'Prabodhasudhakara',
]); ?>
	</ul>
</li>
<li class="menu-item"><a class="menu-link" href="tel:<?php echo am_var('phone') ?>">Call Us</a></li>
<li class="menu-item"><a class="menu-link" href="mailto:<?php echo am_var('email') ?>" target="_blank">Email us</a></li>

<?php function render_menu($menu) {
foreach ($menu as $slug => $text) {?>
  <li class="menu-item"><a class="menu-link" href="<?php echo am_var('url') . $slug; ?>"><?php echo $text; ?></a></li>
<?php } }?>
