<!-- Footer Widgets
============================================= -->
<div class="clearfix" style="z-index: 2">
	<div class="row clearfix">

		<div class="col-lg-12">

			<div class="widget clearfix">
				<div class="row clearfix">
					<div class="col-lg-12 clearfix" style="color: #888;">
						
						<a href="<?php echo am_var('url'); ?>" class="bottommargin-sm"><img src="<?php echo am_var('url'); ?><?php echo am_var('safeName'); ?>-logo.png" alt="<?php echo am_var('name'); ?>" /></a><br />
						<h1><?php echo am_var('name'); ?></h1>
						<p><?php echo am_var('byline'); ?></p>

						<div class="row clearfix">
							<div class="col-6">
								<p><?php echo am_var('footer-message'); ?></p>
							</div>
							<div class="col-6">
								<p><?php echo am_var('footer-message2'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

</div>
