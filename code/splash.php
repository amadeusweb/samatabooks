Liked the picture of <a href="<?php echo am_var('url'); ?>about-lalitha/">Shri Ma Lalithambika</a>? <a id="show-splash" href="#">Show it again.</a>

<script type="text/javascript">
window.pageName = "<?php echo am_var('node'); ?>";
</script>

<div id="splash" style="display: none; position: absolute; height: 100%; width: 100%; left: 0; top: 0; text-align: center; background-color: #ccc;">
  <span style="color: #333; font-weight: bold; padding: 4px; border-radius: 4px;display: inline-block; margin: 5px 0 5px 0;">
    Welcome to Samata Books <span id="splash-timer">10...</span>
  </span><br>
  <audio controls id="homemp3" style="min-width: 400px; width: 40%;">
    <source src="<?php echo am_var('url'); ?>assets/lalitha.mp3" type="audio/mpeg">
    Your browser does not support the audio element.
  </audio>
  <div id="splash-image" style="height: 100%; width: 100%; background: #ccc url(<?php echo am_var('url'); ?>assets/shri-ma-lalithambika.jpg) no-repeat top center; background-size: contain; margin-top: 10px">
</div>
</div>