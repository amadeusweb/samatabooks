<?php
include_once 'functions.php';

$local = $_SERVER['HTTP_HOST'] ==='localhost';

bootstrap(array(
	'name' => 'Samata Books',
	'safeName' => 'samata',
	'byline' => 'Samatvam Yoga Uchyate',
	'footer-message' => '<strong>Contemporary Spirituality</strong><br /><br />At the <u>Personal Book Shop</u> (founded in 1950), exclusively dedicated to exploring worldly enquiry into inner states of meditation and bliss, we cater to all schools of religious and spiritual thought.',
	'footer-message2' => '<strong>Back to the Vedas</strong><br /><br />As humanity expands its consciousness in wells of gratitude, compassion and love, we are looking more and more to ancient Vedic Scriptures for guiding the inner spirit.',

	'version' => [ 'id' => '003', 'date' => '14 May 2022' ],

	'support_page_parameters' => true,

	'robots' => 'noindex',

	'folder' => 'content/',
	'theme' => 'cv-writer',

	'start_year' => '1977',
	'phone' => '+919444010706',
	'email' => 'samatabooks@gmail.com',

	//'styles' => ['styles'],
	'scripts' => ['home'],

	'url' => $local ? 'http://localhost/showcase/samatabooks/' : 'https://showcase.amadeusweb.com/samatabooks/',
	'path' => SITEPATH,
));

//TODO: Is needed?
load_amadeus_module('v2content');

render();
?>
