<?php
function before_render() {
	if (endsWith(am_var('all_page_parameters'), 'embed'))
		am_var('embed', 'true');
}

function before_file() {
	if (am_var('node') == 'index') return;
	echo '<section id="content">
<div class="container">';
}

function after_file() {
	if (am_var('node') == 'index') return;
	echo '</div>
</section>
';
}

function item_r($col, $item, $return = false) {
	$cols = am_var('sectionColumns');

	if ($col == 'image')
		$r = '%url%content/books/' . str_replace(' ', '-', $item[$cols['name']]) . '(' . $item[$cols['sku']] . ').jpg';
	else if ($col == 'link' || $col == 'ajax')
		$r = '%url%' . 'books/' . urlize($item[$cols['name']]) . '/' . ($col == 'ajax' ? 'embed/' : '');
	else
		$r = $item[$cols[$col]];

	if ($col == 'price') $r = $r ? 'Indian Rupees: ' . $r : 'Not Available';

	$r = str_replace('|', '<br />', $r);
	$r = simplify_encoding($r);
	$r = replace_vars($r);
	if ($return) return $r;

	echo $r;
}
?>
