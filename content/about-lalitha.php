<!-- wp:html -->
<audio controls id="homemp3" style="min-width: 400px; width: 40%;"><source src="../assets/lalitha-full.mp3" type="audio/mpeg">Your browser does not support the audio element.</audio>
<p>Download:&nbsp;<a href="../assets/lalitha-full.mp3" target="_blank" rel="noreferrer noopener"><?php echo am_var('url'); ?>assets/lalitha-full.mp3</a></p>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Lalita Sahasranama contains a thousand names of the Hindu mother goddess Lalita.[4] The names are organized in a hymns (stotras). It is the only sahasranama that does not repeat a single name. Further, in order to maintain the meter, sahasranamass use the artifice of adding words like tu, api, ca, and hi, which are conjunctions that do not necessarily add to the meaning of the name except in cases of interpretation. The Lalita sahasranama does not use any such auxiliary conjunctions and is unique in being an enumeration of holy names that meets the metrical, poetical and mystic requirements of a sahasranama by their order throughout the text.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Lalita Sahasranama begins by calling the goddess Shri Mata (the great mother), Shri Maharajni (the great queen) and Shrimat Simhasaneshwari (the queen sitting on the lion-throne).[5] In verses 2 and 3 of the Sahasranama she is described as a Udayatbhanu Sahasrabha (the one who is as bright as the rays of thousand rising suns), Chaturbahu Samanvita (the one who has four hands) and Ragasvarupa Pashadhya (the one who is holding the rope).[6] Chidagnikunda Sambhuta (one who was born from the altar of the fire of consciousness) and Devakarya samudyata (one who manifested Herself for fulfilling the objects of the devas) are among other names mentioned in the sahasranama.</p>
<!-- /wp:paragraph -->