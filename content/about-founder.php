<!-- wp:paragraph -->
<p>VELURY SADANAND <br>
(1918-1994)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Sri Velury Sadanand was born in Chirivada, Andhra Pradesh, in 1918. He passed the Matriculation examination of BENARES HINDU UNIVERSITY with distinction. He lived with The Mahatma Gandhiji for six months in Wardha when he was 19. Later he returned to his village and studied Sanskrit, Hindi and other subjects under his uncle. He participated in the Quit India Movement in 1942 and was imprisoned for six months and kept in Alipuram Jail.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>After his return from Jail, he worked with Dr.Pattabhi Sitaramayya for the All India Congress Committee, Machilipatnam. Later he started his own journal "Samata".</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img class="img-fluid" src="../assets/samata-late-founder-and-wife.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Velury Sadanand and Wife<br>
Sri Velury Sadanand and Smt Velury Vijayalakshmi<br>
After sometime he came to Madras and joined ANDHRA PATRIKA as Journalist. He resigned from Andhra Patrika in 1947 and joined as personal staff of Sri B.Gopala Reddy, the then Finance Minister of Madras Presidency. After three years, he started bookselling with the help of Mr.David Green of Macmillan and Company. He opened his bookshop in 1953, called THE PERSONAL BOOKSHOP at the congress building in Mount Road, (Madras), Chennai, where it continues to function. Till 1977 he was importing Scientific and Technical Books and supplying to various educational instituitions and libraries all over India.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Moltivated by his deep reverence and interest in Indian Culture,Heritage and Religious and Philosophical Traditions, he started Publishing books on such subjects by establishing his own publishing unit called SAMATA BOOKS IN 1977. He was also a Founder Trustee of Indian Heritage Trust along with Sri Jayantilal Parekh of Sri Aurobindo Ashram. Directed and inspired by His Holiness Sri Sankaracharya of Sringeri, he printed and published the COMPLETE WORKS OF ADI SANKARACHARYA in Sanskrit. He also published for the Indian Heritage Trust, VALMIKI RAMAYANA, Eastern Recension, edited by Prof.Gaspare Gorresio and first published in Italy during 1844. Many other spiritual classics have been published by Samata Books with authentic English renderings. In all Samata Books have brought out a total of 30 books and carries on the mission of its founder.</p>
<!-- /wp:paragraph -->