<!-- wp:image -->
<figure class="wp-block-image"><img class="img-fluid" src="../assets/alladi-mahadeva-shastry.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>The text of <a href="../catalogue/#bhagavad-gita" target="_blank" rel="noreferrer noopener" aria-label="the Gita (opens in a new tab)">the Gita</a> as cited by Sankara has come down the centuries as the authentic text and this commentary of his has proved to be of seminal value ever since. Its translation into English by Sri Alladi Mahadeva Sastri has stood the test of time since its first publication in 1897, being the only English translation of Sankara's Gita Bhashya available for 80 years. The late Pandit Mahadeva Sastri was Director, Oriental Section Adyar Library, Curator, Government Oriental Library, Mysore and Fellow of the Theosophical Society. He has also translated Sankara's commentary on Taittiriya Upanishad and Amritabindu, Dakshinamurti Stotra, Pranava Vartika and Dakshinamurti Upanishad besides editing 4 out of 5 volumes of Minor Upanishads for the Adyar Library.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>We consider it a privilege and a Blessing to issue this famous book as our first publication.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>- V Sadanand, Samata Books</p>
<!-- /wp:paragraph -->