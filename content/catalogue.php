<?php
$home = am_var('node') == 'index';
?>
    <!-- ======= Portfolio Section ======= -->
    <section style="margin-top: 40px" id="portfolio" class="portfolio">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2><?php echo $home ? '<a href="catalogue/">Available Books</a>' : 'SamataBooks Catalogue 2020'; ?></h2>
          <p><?php content('catalogue_sub_heading'); ?></p>
          <?php if (!$home) {?><p><a href="<?php echo am_var('url');?>catalogue/">Available Books</a> | <a href="<?php echo am_var('url');?>catalogue/all/">All Books</a></p><?php } ?>
          <p>Contact us with your chosen titles and we will send them to you immediately. Kindly note that shipping will be extra.</p>
        </div>

<div id="post-area" class="masonry">
<?php
$books = get_sheet('catalogue', false);

$available = am_var('page_parameter1') !== 'all';

foreach ($books->rows as $item) {
	$price = if_item_r('price', $item) ? item_r('price', $item, true) : '';
	$description = '***'; // if_item_r('description', $item) ? item_r('description', $item, true) : 'Not Found';

if ($available && $price == '') continue;
?>
          <div class="post">
            <div class="pinbin-image"><img src="<?php item_r('image', $item); ?>" class="img-fluid" alt="<?php item_r('name', $item); ?>"></div>
            <div class="pinbin-category"><?php item_r('sku', $item); ?><br><?php if ($price) { ?>Price: Rs <?php echo $price; ?>/-<?php } else { echo 'Gone for Reprint'; } ?></div>
            <div class="pinbin-copy">
              <h4><?php item_r('name', $item); ?></h4>
              <p><?php echo $description; ?></p>
            </div>
          </div>
<?php } ?>

</div>

      </div>
    </section><!-- End Portfolio Section -->
